package step;

import cucumber.api.java.es.Y;
import net.thucydides.core.annotations.DefaultUrl;
import org.junit.Assert;
import page.LoginInkaFarmaPage;
import utilities.WebDriverManager;

public class LoginInkaFarmaStep {
    LoginInkaFarmaPage loginInkaFarmaPage = new LoginInkaFarmaPage();
    public void unUsuarioCargaLaPaginaWebDeInkaFarma() throws Throwable {
        loginInkaFarmaPage.setDriver(WebDriverManager.setWebDriver("chrome"));
        loginInkaFarmaPage.open();
        loginInkaFarmaPage.getDriver().manage().window().maximize();
        loginInkaFarmaPage.getDriver().getTitle();
        Assert.assertTrue("No se encontro el home de la pagina", loginInkaFarmaPage.validarPagina("Inkafarma: Más salud al mejor precio"));
    }

    public void clickeaEnElBotonDeIniciarSesión() throws Throwable {
        loginInkaFarmaPage.validarPopUpPreciosBajos();
        loginInkaFarmaPage.validadPopUpRegister();
        loginInkaFarmaPage.clickIniciarSesion();
    }

    public void iniciaSesionConExitoYMuestraElHomeDeLaAplicación() throws Throwable {
        Assert.assertTrue("No se encuentra usuario", loginInkaFarmaPage.validarInicioDeSesion());
    }

    /////////////////////////////////////////

    public void clickeaEnElBotonIngresarCon(String sTipo) throws Throwable {
        loginInkaFarmaPage.clickBotonIngresarCon(sTipo);
    }

    public void ingresaSuCorreo(String sUser) throws Throwable {
        loginInkaFarmaPage.ingresarEmail(sUser);
    }

    public void ingresaSuContraseña(String sPassword) throws Throwable {
        loginInkaFarmaPage.ingresarPassword(sPassword);
    }

    public void clickeaEnBotonSiguiente() throws Throwable {
        loginInkaFarmaPage.clickBotonSiguiente();
    }
    public void laAplicacionMuestraUnMensajeDeError(String sMensaje) throws Throwable {
        Assert.assertTrue("No se encuentra Mensaje de error", loginInkaFarmaPage.validarMensajeErrorUsuario(sMensaje));
    }
}
