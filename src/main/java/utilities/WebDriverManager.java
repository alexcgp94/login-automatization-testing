package utilities;


import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.HashMap;


public class WebDriverManager {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";

    public static WebDriver setWebDriver(String browser) {

        System.out.println("Ejecutando en: " + browser);
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        WebDriver webDriver;
        String BASE_PATH = System.getProperty("user.dir");

        if (browser.equalsIgnoreCase("chrome") || browser.equalsIgnoreCase("background")) {

            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.default_directory", BASE_PATH + "\\descargas\\");

            ChromeOptions options = new ChromeOptions();
            if (browser.equalsIgnoreCase("background")) {
                options.addArguments("--headless");
                System.out.println("Modo Background: " + ANSI_GREEN + "SI" + ANSI_RESET);

            } else {
                System.out.println("Modo Background: " + ANSI_RED + "NO" + ANSI_RESET);
            }
            BASE_PATH = BASE_PATH + "/src/main/resources/drivers/win/chrome/chromedriver.exe";
            options.setExperimentalOption("prefs", chromePrefs);
            options.setExperimentalOption("useAutomationExtension", false);


            System.out.println("Las configuraciones se estan iniciando en " + ANSI_BLUE + System.getProperty("os.name") + ANSI_RESET);
            System.setProperty("webdriver.chrome.driver", BASE_PATH);
            webDriver = new ChromeDriver(options);

        } /*else if (browser.equalsIgnoreCase("ie")) {
            BASE_PATH = System.getProperty("user.dir") + "\\";
            BASE_PATH = BASE_PATH + "src/main/resources/driver/IEDriverServer32.exe";
            System.setProperty("webdriver.ie.driver", BASE_PATH);
            webDriver = new InternetExplorerDriver();
        } */else if (browser.equalsIgnoreCase("gecko")) {
            BASE_PATH = BASE_PATH + "/src/main/resources/drivers/win/firefox/geckodriver.exe";

            System.out.println("Las configuraciones se estan iniciando en " + ANSI_BLUE + System.getProperty("os.name") + ANSI_RESET);
            System.setProperty("webdriver.gecko.driver", BASE_PATH);
            webDriver = new FirefoxDriver();
        } else {
            throw new IllegalArgumentException("Tipo de navegador no soportado: " + browser);
        }
        System.out.println("Sistema Opertivo: " + Platform.WINDOWS);
        webDriver.manage().window().maximize();
        return webDriver;

    }

}